# KubeSOS

`kubectl` and `helm` wrapper to retrieve GitLab cluster configuration and logs from GitLab Cloud Native chart deployments

# Not using kubernetes?

We also have [GitLabSOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos), which serves the same purpose for omnibus based gitlab installations.

# What's the relationship between kubeSOS and GitLabSOS?

Both are used to make debugging easier, and attempt to get relevant information from environments. They are not dependencies of each other, just sibling projects!

# I want to add something new to KubeSOS!

You can! Also consider adding it to [GitLabSOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos) as well if it's relevant!

#### Requirements
[kubectl client v1.14+](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

[helm 2.12+](https://helm.sh/docs/intro/quickstart/) 

#### Usage

| Flags | Description | Required   | Default
| :---- | :---------- | :--------- | :------
| `-n`  | namespace   | No | "default"
| `-r`  | helm chart release | No | "gitlab"

Download and execute:

```
chmod +x kubeSOS.sh
./kubeSOS.sh [flags]
```

Or use `curl`:

```
curl https://gitlab.com/gitlab-com/support/toolbox/kubesos/raw/master/kubeSOS.sh | bash -s -- [flags]
```

Data will be archived to `kubesos-<timestamp>.tar.gz`
